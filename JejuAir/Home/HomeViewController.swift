//
//  HomeViewController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/27.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var headerView: TopHeaderView? = nil
    var topBannerView: TopBannerView?  = nil
    var initHeader: Bool = false
    var showTopBanner: Bool = false
    var testTopBanner: Bool = false
    var lastOffset: CGFloat = 0
    
    private var tableContext: UInt8 = 1
    
    private var presenter: HomePresenter? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = HomePresenter(view: self)
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.addObserver(self, forKeyPath: "contentOffset", options: [.old, .new], context: &tableContext)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if initHeader == false || headerView == nil {
            initHeader = true
            topBannerView  = TopBannerView(frame: CGRect(x: 0, y: -40, width: self.view.frame.size.width, height: 40))
            headerView = TopHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40))
            topBannerView?.delegate = self
            headerView?.delegate = self
            self.tableView.addSubview(headerView!)
            self.tableView.addSubview(topBannerView!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context != &tableContext || headerView == nil {
            return
        }
        let newP: CGPoint = change![NSKeyValueChangeKey.newKey] as! CGPoint
        let oldP: CGPoint = change![NSKeyValueChangeKey.oldKey] as! CGPoint
        headerView?.moveTableView(oldOffset: oldP.y, newOffset: newP.y, showTopBanner: showTopBanner)
        topBannerView?.moveTableView(newOffset: newP.y, show: showTopBanner)
    }
    
    func showHideTopBanner(show: Bool) {
        showTopBanner = show
        DispatchQueue.main.async(){
            UIView.animate(withDuration: 0.3, animations: {
                var contentInset: UIEdgeInsets = self.tableView.contentInset
                var bannetRect: CGRect = self.topBannerView?.frame ?? CGRect.zero
                var headerRect: CGRect = self.headerView?.frame ?? CGRect.zero
                contentInset.top = show == true ? 40 : 0
                bannetRect.origin.y = show == true ? bannetRect.origin.y+40 : bannetRect.origin.y-40
                headerRect.origin.y = show == true ? headerRect.origin.y+40 : headerRect.origin.y-40
                self.topBannerView?.frame = bannetRect
                self.headerView?.frame = headerRect
                self.tableView.contentInset = contentInset
            }) { finished in
                if finished == true && show == true {
                    //self.tableView.scrollToRow(at: NSIndexPath(row: 0, section: 0) as IndexPath, at: .top, animated: true)
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = UIColor.brown
        cell.textLabel?.text = "\(indexPath.row)"

        return cell
    }
    
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.showMenu()
        if testTopBanner == true {
            return
        }
        
        testTopBanner = true
        self.showHideTopBanner(show: true)
    }
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != tableView || headerView == nil {
            return
        }
        
        var bShow: Bool = true
        let interval: CGFloat = showTopBanner == true ? -40 : 0
        
        if scrollView.contentOffset.y <= 0 {
            bShow = true
        }
        else {
            bShow = lastOffset < scrollView.contentOffset.y ? false : true
        }
        
        headerView?.showHideAnmation(bShow: bShow, isTop: scrollView.contentOffset.y<=0+interval    )
        lastOffset = scrollView.contentOffset.y
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView != tableView || headerView == nil {
            return
        }
        let interval: CGFloat = showTopBanner == true ? -40 : 0
        
        if scrollView.contentOffset.y != 0 + interval {
            headerView?.changeBackgroundColor(color: UIColor.white)
        }
    }
}

// TopBanner Delegate
extension HomeViewController: TopBannerDelegate {
    func closeTopBanner() {
        testTopBanner = false
        showHideTopBanner(show: false)
    }
    
    func clickTopBanner() {
        //
    }
}

// HeaderView Delegate
extension HomeViewController: TopHeaderDelegate {
    func onMenu() {
        self.showMenu()
    }
    
    func clickLogo() {
        
    }
}

// MVP Delegate
extension HomeViewController: HomeDelegate {
}
