//
//  HomePresnter.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/12/11.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol HomeDelegate {
    
}

class HomePresenter {
    var viewDelegate: HomeDelegate? = nil
    
    required init(view: HomeDelegate?) {
        self.viewDelegate = view
    }
}
