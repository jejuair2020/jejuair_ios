//
//  ViewController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/27.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    // container view
    @IBOutlet weak var naviView: UIView!
    @IBOutlet weak var menuView: UIView!
    
    // childViewController
    var navi: JNaviController?
    var menuVc: MenuViewController?
    
    var showIntro: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // menu view 조정
        self.menuView!.snp.makeConstraints { (make) in
            make.size.equalToSuperview()
            make.top.equalTo(0)
            make.left.equalTo(UIScreen.main.bounds.width)
        }
        
        if showIntro == false {
           showIntro = true
           DispatchQueue.main.async(){
              self.performSegue(withIdentifier: "IntroVC", sender: self)
           }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        debugPrint(segue.identifier ?? "nil")
        if segue.identifier == "containroot" {
            self.navi = segue.destination as? JNaviController
            self.navi?.setParentViewController(vc: self)
        }
        else if segue.identifier == "containmenu" {
            self.menuVc = segue.destination as? MenuViewController
            self.menuVc?.setParentViewController(vc: self)
        }
    }
    
    func showMenu() {
        self.toggleMenu(open: true)
    }
    
    func closeMenu() {
        self.toggleMenu(open: false)
    }
    
    func toggleMenu(open: Bool) {
        DispatchQueue.main.async(){
            UIView.animate(withDuration: 0.3) {
                self.menuView!.snp.updateConstraints { (make) in
                    make.left.equalTo(open == true ? 0 : UIScreen.main.bounds.width)
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func doMenuAction() {
        /*
        guard let tc: JTabBarController = self.navi?.getTabBarController() else {
            self.toggleMenu(open: false)
            return
        }
        
        tc.selectedIndex = 1
        */
        self.toggleMenu(open: false)
        
        let storyboard = UIStoryboard(name: "Other", bundle: nil)
        let controller: AirplaneViewController = storyboard.instantiateViewController(withIdentifier: "AirplaneViewController") as! AirplaneViewController
        
        self.navi?.pushViewController(controller, animated: true)
    }
}

