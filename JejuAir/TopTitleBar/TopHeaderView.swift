//
//  TopHeaderView.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/12/11.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

protocol TopHeaderDelegate {
    func onMenu()
    func clickLogo()
}

class TopHeaderView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var logoImageView: UIImageView!
    var show: Bool = true
    var delegate: TopHeaderDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    private func commonInit(){
        let view = Bundle.main.loadNibNamed("TopHeaderView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.backgroundColor = UIColor.clear
        view.backgroundColor = UIColor.clear
        self.addSubview(view)
        
        //self.logoImageView.isUserInteractionEnabled = true
        //let tap = UITapGestureRecognizer(target: self, action: #selector(tappedLogo(tapGestureRecognizer:)))
        //self.logoImageView.addGestureRecognizer(tap)
    }
    
    func showHideAnmation(bShow: Bool, isTop: Bool) {
        show = bShow
        
        DispatchQueue.main.async(){
            UIView.animate(withDuration: 0.3) {
                self.backgroundColor = isTop == true ? UIColor.clear : UIColor.white
            }
        }
    }
    
    func moveTableView(oldOffset: CGFloat, newOffset: CGFloat, showTopBanner: Bool) {
        if oldOffset == newOffset {
            return
        }
        var rect: CGRect = self.frame
        let interval: CGFloat = showTopBanner == true ? 40 : 0
        // down
        if oldOffset < newOffset {
            if newOffset < rect.size.height + interval {
                rect.origin.y = 0
            }
            else {
                rect.origin.y = newOffset - rect.size.height + interval
            }
        }
        else {
            if newOffset + interval < rect.origin.y {
                rect.origin.y = newOffset + interval
            }
        }
        
        self.frame = rect
    }
    
    func changeBackgroundColor(color: UIColor) {
        self.backgroundColor = color
    }
    
    @IBAction func onMenu(_ sender: Any) {
        if delegate == nil {
            return
        }
        
        delegate?.onMenu()
    }
    
    @objc func tappedLogo(tapGestureRecognizer: UITapGestureRecognizer) {
        if delegate == nil {
            return
        }
        
        delegate?.clickLogo()
    }
}
