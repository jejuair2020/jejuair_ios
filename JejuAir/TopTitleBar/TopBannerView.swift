//
//  TopBannerView.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/12/13.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

protocol TopBannerDelegate {
    func closeTopBanner()
    func clickTopBanner()
}

class TopBannerView: UIView {

    @IBOutlet weak var label: UILabel!
    var show: Bool = false
    var delegate: TopBannerDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    private func commonInit(){
        let view = Bundle.main.loadNibNamed("TopBannerView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
        
        self.label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedLabel(tapGestureRecognizer:)))
        self.label.addGestureRecognizer(tap)
    }
    
    func moveTableView(newOffset: CGFloat, show: Bool) {
        var rect: CGRect = self.frame
        
        rect.origin.y = show == true ? newOffset : newOffset - rect.size.height
        
        self.frame = rect
    }

    @IBAction func onClose(_ sender: Any) {
        if delegate == nil {
            return
        }
        delegate?.closeTopBanner()
    }
    
    @objc func tappedLabel(tapGestureRecognizer: UITapGestureRecognizer) {
        if delegate == nil {
            return
        }
        delegate?.clickTopBanner()
    }
}
