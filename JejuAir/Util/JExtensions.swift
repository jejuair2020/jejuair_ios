//
//  JExtensions.swift
//  testweb
//
//  Created by WONJIN CHOI on 2019/11/06.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

public extension Dictionary {

    func toJSONString() -> String? {
        if let data = try? JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            if let json = String(data: data, encoding: String.Encoding.utf8) {
                return json
            }
        }

        return nil
    }
}
