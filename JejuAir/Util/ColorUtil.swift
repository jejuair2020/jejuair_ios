//
//  ColorUtil.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/14.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    struct JEJUCOLOR {
        static let TABTINT      = UIColor(hex: "#ff5100")
        static let TABSELECTION = UIColor(hex: "#ff5100")
        static let TABTINTUNSELECTED = UIColor(hex: "#212121")
    }
    
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        var hexColor:String = hex

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            hexColor = String(hex[start...])
        }
        let scanner = Scanner(string: hexColor)
        var hexNumber: UInt64 = 0
        
        if scanner.scanHexInt64(&hexNumber) {
            switch hexColor.count {
            case 8:
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                break
            case 6:
                r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                g = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
                b = CGFloat(hexNumber & 0x0000ff) / 255
                a = 1.0
                break
            default:
                r = 1.0
                g = 1.0
                b = 1.0
                a = 1.0
                break
            }
            
            self.init(red: r, green: g, blue: b, alpha: a)
            return
        }

        return nil
    }
    
    public convenience init?(red: Int, green: Int, blue: Int) {
        if red < 0 || red > 255 || green < 0 || green > 255 || blue < 0 || blue > 255 {
            return nil
        }
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}
