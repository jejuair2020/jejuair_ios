//
//  FileUtil.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/20.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation

class FileUtil {
    func saveJsonFile(destFoler: String, fileName: String, saveCont: String) -> Bool {
        let fileManager = FileManager.default
        let destFolerPath = URL(fileURLWithPath: DOCUMENT_FOLDER).appendingPathComponent(destFoler)
        
        // folder 생성
        if fileManager.fileExists(atPath: destFolerPath.path) == false {
            do {
                try fileManager.createDirectory(at: destFolerPath, withIntermediateDirectories: true, attributes: nil)
            }
            catch {
                debugPrint("fail create foler \(destFolerPath.path)")
                return false
            }
        }
        
        let destFilePath = destFolerPath.appendingPathComponent(fileName)
        
        if fileManager.fileExists(atPath: destFilePath.path) == true {
            do {
                try fileManager.removeItem(atPath: destFilePath.path)
            }
            catch {
                debugPrint("fail remove at path \(destFolerPath.path)")
            }
        }
        
        do {
            try saveCont.write(to: destFilePath, atomically: true, encoding: .utf8)
        }
        catch {
            debugPrint("fail remove at path \(destFolerPath.path)")
        }
        
        return true
    }
    
    func restoreFromFile(sFolder: String, fileName: String) -> String {
        let sourceFilePath = URL(fileURLWithPath: DOCUMENT_FOLDER).appendingPathComponent(sFolder).appendingPathComponent(fileName)
        var s = ""
        do {
            s = try String(contentsOf: sourceFilePath, encoding: .utf8)
        }
        catch {
            s = ""
        }
        return s
    }
}
