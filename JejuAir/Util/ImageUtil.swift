//
//  ImageUtil.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/14.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineHeight: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        //UIRectFill(CGRect(origin: CGPoint(x: 0,y :size.height - lineHeight), size: CGSize(width: size.width, height: lineHeight)))
        UIRectFill(CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: size.width, height: lineHeight)))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension UIImageView {
    
    func setImageUrlWithPlaceholder(url: String, withIndicator: Bool) {
        /*
        let image = UIImage(named: "default")
        if withIndicator == true {
            self.kf.indicatorType = .activity
        }
        self.kf.setImage(with: URL(string:url), placeholder: image)
        */
        self.setImageUrl(url: url)
    }
    
    func setImageUrl(url: String) {
        self.kf.setImage(with: URL(string:url)) { result in
            switch result {
            case .success(_):
                break
            case .failure(_):
                //self.image = UIImage(named: "default")
                break
            }
        }
    }
    
    func setImageUrlWIthFadeIn(url: String, sec: Double){
        self.kf.setImage(with: URL(string: url), options: [.transition(.fade(sec))])
    }
    
    func setImageUrlWIthRoundCorner(url: String, radius: CGFloat){
        let processor = RoundCornerImageProcessor(cornerRadius: radius)
        self.kf.setImage(with: URL(string: url), options: [.processor(processor)])
    }
    
    func downloadImage(urlString: String){
        guard let url = URL(string: urlString) else {
            return
        }
        let resource = ImageResource(downloadURL: url)

        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                print("Image: \(value.image). Got from: \(value.cacheType)")
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
}
