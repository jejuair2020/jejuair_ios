//
//  StringUtil.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/14.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation

extension String {
    var length: Int {
        get {
            return self.count
        }
    }
    
    func contains(s: String) -> Bool
    {
        return (self.range(of: s) != nil) ? true : false
    }
    
    func isNilOrEmpty(s: String?) -> Bool {
        guard let txt = s else {
            return true
        }
        
        if txt.count == 0 {
            return true
        }
        
        return false
    }
    
    func isTrimNilOrEmpty(s: String?) -> Bool {
        guard let txt = s else {
            return true
        }
        
        if txt.length == 0 {
            return true
        }
        
        return txt.trimmingCharacters(in: .whitespacesAndNewlines).length == 0
    }
    
    func isEqualToString(s: String?) -> Bool {
        return self == s
    }
}
