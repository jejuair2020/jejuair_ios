//
//  OCRPlugIn.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/12/10.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation

class OCRPlugIn: JejuWebPlugIn {
    static let sharedInstance:OCRPlugIn = OCRPlugIn()
    var callbackId: String? = ""
    
    override func execute(action:String, params:Dictionary<String, Any>?, cbId:String) -> Bool {
        self.callbackId = cbId
        if action == "openOcrCardNumber" {
            self.openOcrCardNumber(cbId: cbId)
            return true
        }
        
        if action == "openOcrPassport" {
            self.openOcrPassport(cbId: cbId)
            return true
        }
        return false
    }
    
    func getOCRController() -> OCRViewController? {
        let storyboard = UIStoryboard(name: "Other", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "OCRViewController") as? OCRViewController
        controller?.delegate = self
        return controller
    }
    
    func openOcrCardNumber(cbId: String) {
        guard let controller: OCRViewController = self.getOCRController() else {
            return
        }
        controller.setPrepareData(sExpiry: true, vNumber: true, vExpiry: true, sType: 0)
        self.parent?.present(controller, animated: true, completion: nil)
    }
    
    func openOcrPassport(cbId: String) {
        guard let controller: OCRViewController = self.getOCRController() else {
            return
        }
        controller.setPrepareData(sExpiry: true, vNumber: true, vExpiry: true, sType: 5)
        self.parent?.present(controller, animated: true, completion: nil)
    }
}

extension OCRPlugIn: OCRViewDelegate {
    func didEndOCRDecode(result: OcrScanResult, scanType: Int32) {
        var retDic = Dictionary<String, Any>()
        retDic["result"] = "success"
        
        // card
        if scanType == 0 {
            retDic["cardNumber"] = result.cardNumber
            retDic["expiryDate"] = result.expiryDate
        }
        // passport
        else {
            retDic["passport_type"] = result.passport_type
            retDic["name"] = result.name
            retDic["name_kor"] = result.name_kor
            retDic["kor_id_num"] = result.kor_id_num
            retDic["issuing_country"] = result.issuing_country
            retDic["passport_no"] = result.passport_no
            retDic["nationality"] = result.nationality
            retDic["date_of_birth"] = result.date_of_birth
            retDic["sex"] = result.sex
            retDic["date_of_issue"] = result.date_of_issue
            retDic["personal_no"] = result.personal_no
            retDic["validate"] = result.validate
        }
        self.sendSuccessCallback(callbackId: self.callbackId!, result: retDic)
    }
}
