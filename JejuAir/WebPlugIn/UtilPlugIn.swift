//
//  UtilPlugIn.swift
//  testweb
//
//  Created by WONJIN CHOI on 2019/11/12.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit
import SafariServices

class UtilPlugIn: JejuWebPlugIn {
    static let sharedInstance:UtilPlugIn = UtilPlugIn()
    
    override func execute(action:String, params:Dictionary<String, Any>?, cbId:String) -> Bool {
        if action == "openSystemWebView" {
            self.openSystemWebView(params: params, cbId: cbId)
            return true
        }
        
        if action == "openInAppWebView" {
            self.openInAppWebView(params: params, cbId: cbId)
            return true
        }
        
        if action == "close" || action == "closeToRoot" {
            if self.parent?.navigationController != nil {
                if action == "close" {
                    self.parent?.navigationController?.popViewController(animated: true)
                }
                else {
                    self.parent?.navigationController?.popToRootViewController(animated: true)
                }
                return true
            }
            
            if self.parent?.tabBarController != nil {
                self.parent?.tabBarController?.dismiss(animated: true, completion: nil)
                return true
            }
            
            self.parent?.dismiss(animated: true, completion: nil)
            return true
        }
        
        if action == "openMenu" {
            guard let navi: JNaviController = self.parent?.navigationController as? JNaviController else {
                return true
            }
            navi.showMenu()
        }
        return false
    }
    
    func openSystemWebView(params: Dictionary<String, Any>?, cbId: String) {
        guard let dic = params else {
            self.sendErrorCallback(callbackId: cbId, msg: HYBRID_ERROR_DESC_0002, code: HYBRID_ERROR_CODE_0002)
            return
        }
        
        let arg1:String = self.getStringParam(params: dic, key: "arg1")
        if arg1.isEmpty == true {
            self.sendErrorCallback(callbackId: cbId, msg: HYBRID_ERROR_DESC_0002, code: HYBRID_ERROR_CODE_0002)
            return
        }
        
        let urlString = self.getLinkUrlString(arg1: arg1)
        guard let lnkUrl = URL(string: urlString) else { return }
        UIApplication.shared.open(lnkUrl)
        self.sendSuccessCallback(callbackId: cbId, result: nil)
    }
    
    func openInAppWebView(params: Dictionary<String, Any>?, cbId: String) {
        guard let dic = params else {
            self.sendErrorCallback(callbackId: cbId, msg: HYBRID_ERROR_DESC_0002, code: HYBRID_ERROR_CODE_0002)
            return
        }
        
        guard let vc = self.getParentViewController() else {
            self.sendErrorCallback(callbackId: cbId, msg: HYBRID_ERROR_DESC_0002, code: HYBRID_ERROR_CODE_0002)
            return
        }
        
        let arg1:String = self.getStringParam(params: dic, key: "arg1")
        if arg1.isEmpty == true {
            self.sendErrorCallback(callbackId: cbId, msg: HYBRID_ERROR_DESC_0002, code: HYBRID_ERROR_CODE_0002)
            return
        }
        
        let urlString = self.getLinkUrlString(arg1: arg1)
        guard let lnkUrl = URL(string: urlString) else { return }
        let safariViewController = SFSafariViewController(url: lnkUrl)
        safariViewController.modalPresentationStyle = .overFullScreen
        vc.present(safariViewController, animated: true, completion: nil)
        
        self.sendSuccessCallback(callbackId: cbId, result: nil)
    }
    
    func getLinkUrlString(arg1:String)->String {
        if arg1.isEmpty == true {
            return ""
        }
        
        var urlString:String = "";
        if arg1.lowercased().hasPrefix("http") {
            urlString = arg1
        }
        else {
            urlString = "http://\(arg1)"
        }
        
        return urlString
    }
}


