//
//  OcrScanResult.h
//  FinCubeOcrSDKv2
//
//  Created by FinCube on 2018. 9. 18..
//  Copyright © 2018년 FinCube. All rights reserved.
//

#ifndef OcrScanResult_h
#define OcrScanResult_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 * @brief   Card scan result information
 * @author  FinCube
 * @date    2018.08.27
 * @version 2.0
 */
@interface OcrScanResult : NSObject

// credit card
@property (nonatomic, readwrite) NSString* CardNumber;
@property (nonatomic, readwrite) NSString* ExpiryDate;
@property (nonatomic, readwrite) UIImage* cardImage;
@property (nonatomic, readwrite) UIImage* cardOrgImage;
@property (nonatomic, readwrite) UIImage* idPhotoImage;
@property (nonatomic, readwrite) NSInteger result;

@property(nonatomic, readwrite) NSString* name;
@property(nonatomic, readwrite) NSString* kor_id_num;

// kor id
@property(nonatomic, readwrite) NSString* license_type;
@property(nonatomic, readwrite) NSString* license_no;
@property(nonatomic, readwrite) NSString* issuing_date;
@property(nonatomic, readwrite) NSString* issuing_region;
@property(nonatomic, readwrite) NSString* license_serial;
@property(nonatomic, readwrite) bool overseas_resident;

// passport
@property(nonatomic, readwrite) NSString* passport_type;
@property(nonatomic, readwrite) NSString* issuing_country;
@property(nonatomic, readwrite) NSString* name_kor;
@property(nonatomic, readwrite) NSString* passport_no;
@property(nonatomic, readwrite) NSString* nationality;
@property(nonatomic, readwrite) NSString* date_of_birth;
@property(nonatomic, readwrite) NSString* sex;
@property(nonatomic, readwrite) NSString* date_of_issue;
@property(nonatomic, readwrite) NSString* personal_no;
@property(nonatomic, readwrite) bool validate;

// type
@property(nonatomic, readwrite) NSInteger scanType;
@property(nonatomic, readwrite) NSMutableArray* scanFrameList;

@end

#endif /* OcrScanResult_h */
