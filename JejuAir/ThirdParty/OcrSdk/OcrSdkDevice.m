//
//  OcrSdkDevice.m
// @FinCube
//


#import "OcrSdkDevice.h"

#import "FinCubeLog.h"

#import <UIKit/UIKit.h>
#import <MobileCoreServices/UTCoreTypes.h>
#include <sys/sysctl.h>

#pragma mark -

@interface OcrSdkDevice ()

+ (NSString *)getSysInfoByName:(char *)infoSpecifier;
+ (BOOL)is3GS;

@end

@implementation OcrSdkDevice

+ (NSString *)getSysInfoByName:(char *)infoSpecifier {
	size_t size;
  sysctlbyname(infoSpecifier, NULL, &size, NULL, 0);
  char *answer = malloc(size);
	sysctlbyname(infoSpecifier, answer, &size, NULL, 0);
	NSString *result = [NSString stringWithCString:answer encoding:NSUTF8StringEncoding];
	free(answer);
	return result;
}

+ (NSString *)platformName {
  return [self getSysInfoByName:"hw.machine"];
}

+ (BOOL)is3GS {
  NSString *platformName = [self platformName];
  InfoLog(@"Platform name is %@", platformName);
  BOOL is3GS = [platformName hasPrefix:@"iPhone2"]; // should this be @"iPhone2,", so we don't pickup the 20th gen iPhone? :)
  return is3GS;
}

+ (BOOL)hasVideoCamera {
  // check for a camera
  if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    return NO;
  }
  
  // check for video support
  NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
  BOOL supportsVideo = [availableMediaTypes containsObject:(NSString *)kUTTypeMovie];
  
  
  return supportsVideo;
}

+ (BOOL)shouldSetPixelFormat {
  // The 3GS chokes when you set the pixel format!?
  // Fortunately, the default is the one we want anyway.
  return ![self is3GS];
}

@end
