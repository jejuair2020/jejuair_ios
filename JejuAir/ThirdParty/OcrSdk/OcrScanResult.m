//
//  OcrScanResult.m
//  FinCubeOcrSDK
//
//  Created by FinCube on 2018. 9. 18..
//  Copyright © 2018년 FinCube. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OcrScanResult.h"

@interface OcrScanResult()

@end

@implementation OcrScanResult

- (id)init {
    self = [super init];
    
    return self;
}

- (void)dealloc {
    
}

@end
