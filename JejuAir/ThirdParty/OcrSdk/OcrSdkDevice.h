//
//  OcrSdkDevice.h
// @FinCube
//

// Repository for device-specific settings. Device includes both hardware and OS version.

#import <Foundation/Foundation.h>

@interface OcrSdkDevice : NSObject

+ (BOOL)hasVideoCamera;
+ (BOOL)shouldSetPixelFormat;

@end
