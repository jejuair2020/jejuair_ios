//
//  OcrSdkViewDelegate.h
//  FinCube
//

#import <Foundation/Foundation.h>

@class OcrScanResult;
@class OcrSdkView;

/// The receiver will be notified when the OcrSdkView completes it work.
@protocol OcrSdkViewDelegate<NSObject>

@required

/// This method will be called when the OcrSdkView completes its work.
/// It is up to you to hide or remove the OcrSdkView.
/// At a minimum, you should give the user an opportunity to confirm that the card information was captured correctly.
/// @param ocrSDKView The active OcrSdkView.
/// @param result The results of the scan.
/// @note cardInfo will be nil if exiting due to a problem (e.g., no available camera).
- (void)ocrSDKView:(OcrSdkView *)ocrSDKView didScanCard:(OcrScanResult *)result;

@end

