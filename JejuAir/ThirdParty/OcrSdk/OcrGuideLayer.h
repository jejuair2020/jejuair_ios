//
//  OcrGuideLayer.h
//  FinCubeOcrSDKv2
//
//  Created by FinCube on 2018. 9. 12..
//  Copyright © 2018년 FinCube. All rights reserved.
//

#ifndef OcrGuideLayer_h
#define OcrGuideLayer_h

#import <UIKit/UIKit.h>

/**
 * @brief   Caemra Overlay layer
 * @details Display Credit card guide on the camera preview
 * @author  FinCube
 * @date    2018.08.27
 * @version 2.0
 */
@interface OcrGuideLayer : CALayer

/**
 * @details add guide layer to input layer
 * @param layer a layer to own guide layer
 */
- (BOOL)addLayer:(CALayer*)layer;
/**
 * @details set guide layer layout
 * @param rect it must be same with camera preview layout
 */
- (BOOL)layerLayout:(CGRect)rect;
/**
 * @details set credit card guide area
 * @param guiderect it must calculated with camera preview getGuideRect fucntion
 */
- (BOOL)configureGuideLayer:(CGRect)guiderect;
/**
 * @details
 * @param 
 */
- (void)changeGuideLayerState:(NSInteger)state;

@end

#endif /* OcrGuideLayer_h */
