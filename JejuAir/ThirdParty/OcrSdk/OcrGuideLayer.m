//
//  OcrGuideLayer.m
//  FinCubeOcrSDK
//
//  Created by FinCube on 2018. 9. 12..
//  Copyright © 2018년 FinCube. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OcrGuideLayer.h"

@interface OcrGuideLayer()

@property(nonatomic, readwrite) CAShapeLayer *defaultOverlay;
@property(nonatomic, readwrite) CGRect guideRect;

@end

@implementation OcrGuideLayer

- (id)init {
    if((self = [super init])) {
        self.defaultOverlay = [CAShapeLayer layer];
    }
    
    return self;
}

- (BOOL)addLayer:(CALayer*)layer {
    [layer addSublayer:self.defaultOverlay];
    return TRUE;
}

- (BOOL)layerLayout:(CGRect)rect {
    self.defaultOverlay.frame = rect;
    return TRUE;
}

- (BOOL)configureGuideLayer:(CGRect)guiderect {
    self.guideRect = guiderect;
    return TRUE;
}

- (void)changeGuideLayerState:(NSInteger)state {
    UIBezierPath *guidebox = [UIBezierPath bezierPathWithRoundedRect:self.guideRect cornerRadius:15.];
    UIBezierPath *boundbox = [UIBezierPath bezierPathWithRect:self.defaultOverlay.bounds];

    [boundbox appendPath:guidebox];
    [boundbox setUsesEvenOddFillRule:YES];
    self.defaultOverlay.path = boundbox.CGPath;
    
    if( state == 0 )
        self.defaultOverlay.fillColor = [UIColor colorWithWhite:1.0f alpha:0.2f].CGColor;
    else if( state == 1 )
        self.defaultOverlay.fillColor = [UIColor colorWithWhite:0.0f alpha:0.4f].CGColor;
    
    self.defaultOverlay.fillRule = kCAFillRuleEvenOdd;
    
    self.defaultOverlay.strokeColor = [UIColor colorWithWhite:1.0f alpha:0.7f].CGColor;
    //self.defaultOverlay.lineWidth = 4.0;
}

@end
