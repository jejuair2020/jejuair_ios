//
//  FinCubeLog.h
//  FinCubeOcrSDKv2
//
//  Created by FinCube on 2018. 9. 11..
//  Copyright © 2018년 FinCube. All rights reserved.
//

#ifndef FinCubeLog_h
#define FinCubeLog_h

//
// LOG on/off flag
//
#define FinCubeLOG              1
#define FinCube_INFO            1
#define FinCube_DEBUG           1
#define FinCube_WARNNING        1
#define FinCube_ERROR           1

#if FinCube_INFO
#define InfoLog(s,...) NSLog(@"[INFO : %@ %s(%d)] %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__]);
#else
#define InfoLog(s,...)
#endif // FinCube_INFO

#if FinCube_DEBUG
#define DebugLog(s,...) NSLog(@"[DEBUG : %@ %s(%d)] %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__]);
#else
#define DebugLog(s,...)
#endif // FinCube_DEBUG

#if FinCube_WARNNING
#define WarnningLog(s,...) NSLog(@"[WARNNING : %@ %s(%d)] %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__]);
#else
#define WarnningLog(s,...)
#endif // FinCube_WARNNING

#if FinCube_ERROR
#define ErrorLog(s,...) NSLog(@"[ERROR : %@ %s(%d)] %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__]);
#else
#define ErrorLog(s,...)
#endif // FinCube_ERROR

#endif /* FinCubeLog_h */
