//
//  OcrSdkConfig.h
//  Fingram
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 * @brief   card scanner setting & handle
 * @author  Fingram
 * @date    2018.08.27
 * @version 2.0
 */
@interface OcrSdkConfig : NSObject


//
// system info
//
@property(nonatomic, assign, readwrite) void* handle;
@property(nonatomic, assign, readwrite) CGRect guideRect;
@property(nonatomic, assign, readwrite) int previewWidth;
@property(nonatomic, assign, readwrite) int previewHeight;

//
// custom guide rect info
//
@property(nonatomic, assign, readwrite) int guide_x;
@property(nonatomic, assign, readwrite) int guide_y;
@property(nonatomic, assign, readwrite) int guide_w;
@property(nonatomic, assign, readwrite) int guide_h;

//
// credit card result info
//
@property(nonatomic, readwrite) NSString* cardNumber;
@property(nonatomic, readwrite) int expiryYear;
@property(nonatomic, readwrite) int expiryMonth;
@property(nonatomic, readwrite) UIImage* cardImage;

//
// id
//
@property(nonatomic, readwrite) NSString* name;
@property(nonatomic, readwrite) NSString* kor_id_num;
@property(nonatomic, readwrite) UIImage* cardOrgImage;
@property(nonatomic, readwrite) UIImage* idPhotoImage;
//
// kor id
//
@property(nonatomic, readwrite) NSString* license_type;
@property(nonatomic, readwrite) NSString* license_no;
@property(nonatomic, readwrite) NSString* license_serial;
@property(nonatomic, readwrite) NSString* issuing_date;
@property(nonatomic, readwrite) NSString* issuing_region;
@property(nonatomic, readwrite) bool overseas_resident;

//
// passport result info
//
//@property(nonatomic, readwrite) int numbers[16];
@property(nonatomic, readwrite) NSString* passport_type;
@property(nonatomic, readwrite) NSString* issuing_country;
@property(nonatomic, readwrite) NSString* name_kor;
@property(nonatomic, readwrite) NSString* passport_no;
@property(nonatomic, readwrite) NSString* nationality;
@property(nonatomic, readwrite) NSString* date_of_birth;
@property(nonatomic, readwrite) NSString* sex;
@property(nonatomic, readwrite) NSString* date_of_issue;
@property(nonatomic, readwrite) NSString* personal_no;
@property(nonatomic, readwrite) bool validate;

@property(nonatomic, readwrite) NSMutableArray* scanFrameList;

@end
