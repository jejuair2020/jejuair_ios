//
//  OcrCameraPreview.m
//  FinCubeOcrSDK
//
//  Created by FinCube on 2018. 9. 11..
//  Copyright © 2018년 FinCube. All rights reserved.
//

#import "OcrCameraPreview.h"

#import "OcrSdkDevice.h"
#import "FinCubeLog.h"

#import "OcrScanResult.h"

#import "FinCubeOcrSDK/OcrSdkVideoFrame.h"

#import <Foundation/Foundation.h>

#define kVideoQueueName "ocr.sdk.ios.videostream"

// test in camerapreview
#import "FinCubeOcrSDK/OcrSdkCardScanner.h"
#import "FinCubeOcrSDK/OcrSdkConfig.h"

@interface OcrCameraPreview()

@property (strong, nonatomic, readwrite) OcrSdkCardScanner*     ocrScanner;
@property (strong, nonatomic, readwrite) OcrSdkConfig*          config;

@property(nonatomic, assign, readwrite) BOOL previewOnGoing;

@property(nonatomic, strong, readwrite) AVCaptureVideoPreviewLayer  *preview;
@property(nonatomic, strong, readwrite) AVCaptureDevice             *camera;
@property(nonatomic, strong, readwrite) AVCaptureDeviceInput        *cameraInput;
@property(nonatomic, strong, readwrite) AVCaptureVideoDataOutput    *videoOutput;
@property(nonatomic, strong, readwrite) AVCaptureSession            *captureSession;
// This semaphore is intended to prevent a crash which was recorded with this exception message:
// "AVCaptureSession can't stopRunning between calls to beginConfiguration / commitConfiguration"
@property(nonatomic, strong, readwrite) dispatch_semaphore_t cameraConfigurationSemaphore;

@property (nonatomic, assign, readwrite) BOOL           currentlyAdjustingFocus;
@property (nonatomic, assign, readwrite) BOOL           currentlyAdjustingExposure;

@property (nonatomic, readwrite) BOOL scanExpiry;
@property (nonatomic, readwrite) BOOL validateNumber;
@property (nonatomic, readwrite) BOOL validateExpiry;

@property (nonatomic, readwrite) BOOL scanIssuingDate;
@property (nonatomic, readwrite) BOOL scanIssuingRegion;
@property (nonatomic, readwrite) BOOL scanLicenseNumber;
@property (nonatomic, readwrite) BOOL scanLicenseType;
@property (nonatomic, readwrite) BOOL scanLisenseSerial;

@property(nonatomic, assign, readwrite) int ocrScannerType;
//@property(assign, readwrite) int scannerType;

@end

@implementation OcrCameraPreview

- (id)init {
    if( (self = [super init]) ) {
        // open camera & set preview
        self.captureSession = [[AVCaptureSession alloc] init];
        self.camera         = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        self.preview        = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
        
        self.preview.needsDisplayOnBoundsChange = TRUE;
        self.preview.contentsGravity = kCAGravityResizeAspectFill;
        self.preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
        self.preview.backgroundColor = [UIColor grayColor].CGColor;
        
        // parameter of `1` implies "allow access to only one thread at a time"
        self.cameraConfigurationSemaphore = dispatch_semaphore_create(1);
        self.previewOnGoing  = FALSE;
    }
    
    return self;
}

- (AVCaptureVideoPreviewLayer*) getPreview{
    //printf("OcrCameraPreview.m - getPreview\n");
    return self.preview;
}

-(void) moveGuideRect:(float)horizontal_offset vertical:(float)vertical_offset {
    self.horizontal_offset = horizontal_offset;
    self.vertical_offset = vertical_offset;
}

- (void)startSession {
    if( self.previewOnGoing == TRUE )
        return;
    
    //self.ocrScanner = [[OcrSdkCardScanner alloc] init];
    self.ocrScanner = [[OcrSdkCardScanner alloc] init:self.ocrScannerType];
    self.config     = [[OcrSdkConfig alloc] init];
    
    [self.ocrScanner configureScanner:self.ocrScannerType config:self.config];
    
    int ret = [self.ocrScanner moveGuideRect:self.config horizontal:self.horizontal_offset vertical:self.vertical_offset];
    
    NSLog(@"moveGuideRect return : %d", ret);
  
    // option
    switch(self.ocrScannerType) {
            
        case 0:
            [self.ocrScanner setScanOption:self.config scanExpiry:(BOOL)self.scanExpiry validateNumber:(BOOL)self.validateNumber validateExpiry:(BOOL)self.validateExpiry];
            break;
        case 1:
            [self.ocrScanner setScanIDOption:self.config scanIssuingDate:self.scanIssuingDate scanIssuingRegion:self.scanIssuingRegion scanLicenseNumber:self.scanLicenseNumber scanLicenseType:self.scanLicenseType scanLisenseSerial:self.scanLisenseSerial];
            break;
        case 4:
            break;
        case 5:
            break;
    }
    
    if ([self addInputAndOutput]) {
        [self.camera addObserver:self forKeyPath:@"adjustingFocus" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial) context:nil];
        [self.camera addObserver:self forKeyPath:@"adjustingExposure" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial) context:nil];
        [self.captureSession startRunning];
        [self changeCameraConfiguration:^{
            if ([self.camera respondsToSelector:@selector(isAutoFocusRangeRestrictionSupported)]) {
                if(self.camera.autoFocusRangeRestrictionSupported) {
                    self.camera.autoFocusRangeRestriction = AVCaptureAutoFocusRangeRestrictionNear;
                }
            }
            if ([self.camera respondsToSelector:@selector(isFocusPointOfInterestSupported)]) {
                if(self.camera.focusPointOfInterestSupported) {
                    self.camera.focusPointOfInterest = CGPointMake(0.5, 0.5);
                }
            }
            [self.camera setExposureTargetBias:0.25f completionHandler:nil];
        }];
        self.previewOnGoing = TRUE;
    }
}

- (void)stopSession {
    if (self.previewOnGoing) {
        self.previewOnGoing = FALSE;
        
        [self changeCameraConfiguration:^{
            // restore default focus range
            if ([self.camera respondsToSelector:@selector(isAutoFocusRangeRestrictionSupported)]) {
                if(self.camera.autoFocusRangeRestrictionSupported) {
                    self.camera.autoFocusRangeRestriction = AVCaptureAutoFocusRangeRestrictionNone;
                }
            }
        }];
        
        dispatch_semaphore_wait(self.cameraConfigurationSemaphore, DISPATCH_TIME_FOREVER);
        
        [self.camera removeObserver:self forKeyPath:@"adjustingExposure"];
        [self.camera removeObserver:self forKeyPath:@"adjustingFocus"];
        [self.captureSession stopRunning];
        [self removeInputAndOutput];

        dispatch_semaphore_signal(self.cameraConfigurationSemaphore);
        
        [self.ocrScanner destroyScanner:self.ocrScannerType config:self.config];
    }
}

- (BOOL)addInputAndOutput {
    NSError *sessionError = nil;
    self.cameraInput = [AVCaptureDeviceInput deviceInputWithDevice:self.camera error:&sessionError];

    if(sessionError || !self.cameraInput) {
        ErrorLog(@"OcrSdk camera input error : %@", sessionError);
        return FALSE;
    }

    [self.captureSession addInput:self.cameraInput];
    
    if ([self.captureSession canSetSessionPreset:AVCaptureSessionPreset1920x1080]) {
        self.captureSession.sessionPreset = AVCaptureSessionPreset1920x1080;
        InfoLog(@"OcrSdk camera preview size : AVCaptureSessionPreset1920x1080");
    }
    else {
        self.captureSession.sessionPreset = AVCaptureSessionPreset640x480;
        InfoLog(@"OcrSdk camera preview size : AVCaptureSessionPreset640x480");
    }
    self.videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    if( [OcrSdkDevice shouldSetPixelFormat] ) {
        NSDictionary *videoOutputSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithUnsignedInteger:kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange] forKey:(NSString*)kCVPixelBufferPixelFormatTypeKey];
        [self.videoOutput setVideoSettings:videoOutputSettings];
    }
    
    self.videoOutput.alwaysDiscardsLateVideoFrames = YES;
    
    // NB: DO NOT USE minFrameDuration. minFrameDuration causes focusing to
    // slow down dramatically, which causes significant ux pain.
    dispatch_queue_t queue = dispatch_queue_create(kVideoQueueName, DISPATCH_QUEUE_SERIAL);
    [self.videoOutput setSampleBufferDelegate:self queue:queue];
    
    [self.captureSession addOutput:self.videoOutput];
    return TRUE;
}

- (void)removeInputAndOutput {
    [self.captureSession removeInput:self.cameraInput];
    [self.videoOutput setSampleBufferDelegate:nil queue:NULL];
    [self.captureSession removeOutput:self.videoOutput];
}

- (BOOL)changeCameraConfiguration:(void(^)(void))changeBlock {
    dispatch_semaphore_wait(self.cameraConfigurationSemaphore, DISPATCH_TIME_FOREVER);
    
    BOOL success = NO;
    NSError *lockError = nil;
    [self.captureSession beginConfiguration];
    [self.camera lockForConfiguration:&lockError];
    if(!lockError) {
        changeBlock();
        [self.camera unlockForConfiguration];
        success = YES;
    }

    [self.captureSession commitConfiguration];
    
    dispatch_semaphore_signal(self.cameraConfigurationSemaphore);
    
    return success;
}

- (void)setScanOption:(BOOL)scanexpiry validateNumber:(BOOL)validatenumber validateEpiry:(BOOL)validateexpiry {
    self.scanExpiry = scanexpiry;
    self.validateNumber = validatenumber;
    self.validateExpiry = validateexpiry;
}

- (void)setScanIDOption:(BOOL)scanIssuingDate scanIssuingRegion:(BOOL)scanIssuingRegion scanLicenseNumber:(BOOL)scanLicenseNumber scanLicenseType:(BOOL)scanLicenseType scanLicenseSerial:(BOOL)scanLicenseSerial {
    
    self.scanIssuingDate = scanIssuingDate;
    self.scanIssuingRegion = scanIssuingRegion;
    self.scanLicenseNumber = scanLicenseNumber;
    self.scanLicenseType = scanLicenseType;
    self.scanLisenseSerial = scanLicenseSerial;
}

- (void)setScannerType:(int)type {
    self.ocrScannerType = type;
}

// pause /resume control
- (void)didReceiveBackgroundingNotification:(NSNotification *)notification {
    [self stopSession];
}

- (void)didReceiveForegroundingNotification:(NSNotification *)notification {
    [self startSession];
}

// key-value observingh
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    //printf("OcrCameraPreview.m - observeValueForKeyPath\n");
    if ([keyPath isEqualToString:@"adjustingFocus"]) {
        self.currentlyAdjustingFocus = [change[NSKeyValueChangeNewKey] boolValue];
    }
    else if ([keyPath isEqualToString:@"adjustingExposure"]) {
        self.currentlyAdjustingExposure = [change[NSKeyValueChangeNewKey] boolValue];
    }
}

//
// AVCaptureVideoOutputDelegate functions
//
- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection {
    @autoreleasepool {
        
        if( !self.previewOnGoing || sampleBuffer == nil ) {
            NSLog(@"Preview stopped!!!!!!!!!!!!!!!");
            return;
        }

        OcrScanResult* result = [[OcrScanResult alloc] init];
        
        OcrSdkVideoFrame *frame = [[OcrSdkVideoFrame alloc] initWithSampleBuffer:sampleBuffer];
        
        int ret = 0;
        
        switch(self.ocrScannerType) {
            case 0:
                ret = [self.ocrScanner scanFrame:frame config:self.config];

                result.CardNumber = self.config.cardNumber;
                if( self.config.expiryYear < 0 || self.config.expiryMonth < 0 ) {
                    result.ExpiryDate = @"No Expiry";
                }
                else {
                    result.ExpiryDate = [NSString stringWithFormat:@"%02d / %04d", self.config.expiryMonth, self.config.expiryYear];
                }
                
                result.cardImage = self.config.cardImage;
                
                break;
            case 1:
                ret = [self.ocrScanner scanKorIDFrame:frame config:self.config];
                
                result.cardImage = self.config.cardImage;
                result.cardOrgImage = self.config.cardOrgImage;
                result.idPhotoImage = self.config.idPhotoImage;
                
                result.name = self.config.name;
                result.kor_id_num = self.config.kor_id_num;
                result.license_type = self.config.license_type;
                result.license_no = self.config.license_no;
                result.license_serial = self.config.license_serial;
                result.issuing_date = self.config.issuing_date;
                result.issuing_region = self.config.issuing_region;
                result.overseas_resident = self.config.overseas_resident;
                result.scanFrameList = self.config.scanFrameList;
                result.validate = self.config.validate;
                
                break;
            case 4:
                ret = [self.ocrScanner scanAlienFrame:frame config:self.config];
                
                result.cardImage = self.config.cardImage;
                
                result.name = self.config.name;
                result.kor_id_num = self.config.kor_id_num;
                result.issuing_date = self.config.issuing_date;
                
                break;
            case 5:
                ret = [self.ocrScanner scanPassportFrame:frame config:self.config];
                
                result.cardImage = self.config.cardImage;
                result.cardOrgImage = self.config.cardOrgImage;
                
                result.passport_type = self.config.passport_type;
                result.name = self.config.name;
                result.name_kor = self.config.name_kor;
                result.kor_id_num = self.config.kor_id_num;
                result.issuing_country = self.config.issuing_country;
                result.passport_no = self.config.passport_no;
                result.nationality = self.config.nationality;
                result.date_of_birth = self.config.date_of_birth;
                result.sex = self.config.sex;
                result.date_of_issue = self.config.date_of_issue;
                result.personal_no = self.config.personal_no;
                result.validate = self.config.validate;
                
                result.scanFrameList = self.config.scanFrameList;
                
                break;
        }
        
        result.result = ret;
        
        [self performSelectorOnMainThread:@selector(sendFrameToDelegate:) withObject:result waitUntilDone:NO];
    }
}

- (void)sendFrameToDelegate:(OcrScanResult *)result {
    //printf("OcrCameraPreview.m - sendFrameToDelegate\n");
    if( self.previewOnGoing ) {
        if( self.videoFrameDelegate != nil )
            [self.videoFrameDelegate scanResult:result];
    }
}

- (CGRect)getguideRect {
    //printf("OcrCameraPreview.m - getguideRect\n");
    if( self.config == nil || self.ocrScanner == nil )
        return CGRectMake(0, 0, 0, 0);
    
    [self.ocrScanner getGuideFrameRect:self.config width:self.preview.frame.size.width height:self.preview.frame.size.height];
    return self.config.guideRect;
}
@end
