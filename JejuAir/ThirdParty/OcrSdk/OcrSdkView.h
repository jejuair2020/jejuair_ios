//
//  OcrSdkView.h
//  FinCube
//

#import <UIKit/UIKit.h>

#import "OcrSdkViewDelegate.h"

/**
 * @brief   Card Scanner View
 * @details View added to the storyboard, Class that connects the camera and operates the card scanner.
 * @author  FinCube
 * @date    2018.08.27
 * @version 2.0
 */
IB_DESIGNABLE
@interface OcrSdkView : UIView

// delegate to FinCubeCardReaderViewController
@property (nonatomic, assign, readwrite) UIResponder<OcrSdkViewDelegate> *sdkViewDelegate;

/**
 * @brief   Set scanner options
 * @details Set scanner functions that are follows
 * @param scanexpiry expiry scaner on/off
 * @param validatenumber Credit card number luhn check algorithm on/off
 * @param validateexpiry Check expiry date is over or not
 */
- (void)setOptions:(BOOL)scanexpiry validateNumber:(BOOL)validatenumber validateExpiry:(BOOL)validateexpiry;

- (void)setIDOptions:(BOOL)scanIssuingDate scanIssuingRegion:(BOOL)scanIssuingRegion scanLicenseNumber:(BOOL)scanLicenseNumber scanLicenseType:(BOOL)scanLicenseType scanLisenseSerial:(BOOL)scanLisenseSerial;

- (void)setScannerType:(int) scannerType;

@property (nonatomic, assign, readwrite) float horizontal_offset;
@property (nonatomic, assign, readwrite) float vertical_offset;

-(void) moveGuideRect:(float)horizontal_offset vertical:(float)vertical_offset;

@end

