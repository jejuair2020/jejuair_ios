//
//  OcrCameraPreview.h
//  FinCubeOcrSDK
//
//  Created by FinCube on 2018. 9. 11..
//  Copyright © 2018년 FinCube. All rights reserved.
//

#ifndef OcrCameraPreview_h
#define OcrCameraPreview_h

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

#import "OcrCameraPreviewDelegate.h"

/**
 * @brief   CaemraPreview Class
 * @details Open Camera & connect card scanner
 * @author  FinCube
 * @date    2018.08.27
 * @version 2.0
 */
@interface OcrCameraPreview : NSObject<AVCaptureVideoDataOutputSampleBufferDelegate>

/**
 * @details return camera layerout to called object.
 */
- (AVCaptureVideoPreviewLayer*) getPreview;

//
// camera session control
//

/**
 * @details start camera & start card scanner
 */
- (void)startSession;
/**
 * @details close camera & destroy card scanner
 */
- (void)stopSession;

/**
 * @details calculate guide box of cardit card area by iPhone display layer bounds
 */
- (CGRect)getguideRect;

@property (nonatomic, assign, readwrite) float horizontal_offset;
@property (nonatomic, assign, readwrite) float vertical_offset;

-(void) moveGuideRect:(float)horizontal_offset vertical:(float)vertical_offset;

- (void)setScanOption:(BOOL)scanexpiry validateNumber:(BOOL)validatenumber validateEpiry:(BOOL)validateexpiry;
- (void)setScanIDOption:(BOOL)scanIssuingDate scanIssuingRegion:(BOOL)scanIssuingRegion scanLicenseNumber:(BOOL)scanLicenseNumber scanLicenseType:(BOOL)scanLicenseType scanLicenseSerial:(BOOL)scanLicenseSerial;

- (void)setScannerType:(int)type;

// delegate for send output video frames
@property (nonatomic, assign, readwrite) UIResponder<OcrCameraPreviewDelegate> *videoFrameDelegate;

@end

#endif /* OcrCameraPreview_h */
