//
//  OcrSdkView.m
//  FinCube
//

#import "OcrSdkView.h"

#import "OcrSdkViewDelegate.h"

#import "FinCubeLog.h"

#import "OcrCameraPreview.h"
#import "OcrGuideLayer.h"
#import "OcrScanResult.h"

#import "FinCubeOcrSDK/OcrSdkCardScanner.h"
#import "FinCubeOcrSDK/OcrSdkConfig.h"

#import <Foundation/Foundation.h>

@interface OcrSdkView () <OcrCameraPreviewDelegate>

@property (strong, nonatomic, readwrite) OcrCameraPreview* preview;
@property (strong, nonatomic, readwrite) OcrGuideLayer* guidelayer;

@property (strong, nonatomic, readwrite) OcrSdkConfig* config;

@property (strong, nonatomic, readwrite) UIImageView *debugView;

@property (nonatomic, readwrite) BOOL scanExpiry;
@property (nonatomic, readwrite) BOOL validateNumber;
@property (nonatomic, readwrite) BOOL validateExpiry;

@property (nonatomic, readwrite) BOOL scanIssuingDate;
@property (nonatomic, readwrite) BOOL scanIssuingRegion;
@property (nonatomic, readwrite) BOOL scanLicenseNumber;
@property (nonatomic, readwrite) BOOL scanLicenseType;
@property (nonatomic, readwrite) BOOL scanLisenseSerial;

//@property (strong, nonatomic, readwrite) BOOL scanExpiry;
//@property (strong, nonatomic, readwrite) BOOL validateNumber;
//@property (strong, nonatomic, readwrite) BOOL validateExpiry;

@property(nonatomic, assign, readwrite) int cardType; // creditCard : 0, kor_id : 1, alien : 4, passport : 5

@end

@implementation OcrSdkView

- (instancetype)initWidthCoder:(NSCoder*)coder
{
    if (self = [super initWithCoder:coder]){
        
    }
    return self;
}

- (void)willMoveToWindow:(UIWindow *)newWindow {
    if (!newWindow) {
        [self implicitStop];
    }
    [super willMoveToWindow:newWindow];
}

- (void)didMoveToWindow {
    [super didMoveToWindow];
    if (self.window) {
        [self implicitStart];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.preview getPreview].frame = self.bounds;
    [self.guidelayer layerLayout:self.bounds];
    [self.guidelayer changeGuideLayerState:0];

    CGRect guiderect = [self.preview getguideRect];
    [self.guidelayer configureGuideLayer:guiderect];
}

-(void) moveGuideRect:(float)horizontal_offset vertical:(float)vertical_offset {
    self.horizontal_offset = horizontal_offset;
    self.vertical_offset = vertical_offset;
}

- (void)implicitStart {
    self.config     = [[OcrSdkConfig alloc] init];
    
    self.preview = [[OcrCameraPreview alloc] init];
    self.guidelayer = [[OcrGuideLayer alloc] init];
    
    [self.preview setScannerType:self.cardType];
    [self.preview setScanOption:self.scanExpiry validateNumber:self.validateNumber validateEpiry:self.validateExpiry];
    [self.preview setScanIDOption:self.scanIssuingDate scanIssuingRegion:self.scanIssuingRegion scanLicenseNumber:self.scanLicenseNumber scanLicenseType:self.scanLicenseType scanLicenseSerial:self.scanLisenseSerial];
    
    [self.preview moveGuideRect:self.horizontal_offset vertical:self.vertical_offset];
    
    //[self.preview setScanIDOption:self.scanExpiry validateNumber:self.validateNumber validateEpiry:self.validateExpiry];
    //InfoLog(@"ui options : scanExpiry %d, validateNumber %d, validateEpiry %d", self.scanExpiry, self.validateNumber, self.validateExpiry);
    CALayer* camerapreview = [self.preview getPreview];
    [self.layer addSublayer:camerapreview];
    [self.guidelayer addLayer:self.layer];
    
    self.preview.videoFrameDelegate = self;
    [self.preview startSession];
}

- (void)implicitStop {
    [self.preview stopSession];
}

- (void)setOptions:(BOOL)scanexpiry validateNumber:(BOOL)validatenumber validateExpiry:(BOOL)validateexpiry {
    self.scanExpiry = scanexpiry;
    self.validateNumber = validatenumber;
    self.validateExpiry = validateexpiry;
}

- (void)setIDOptions:(BOOL)scanIssuingDate scanIssuingRegion:(BOOL)scanIssuingRegion scanLicenseNumber:(BOOL)scanLicenseNumber scanLicenseType:(BOOL)scanLicenseType scanLisenseSerial:(BOOL)scanLisenseSerial {
    self.scanIssuingDate = scanIssuingDate;
    self.scanIssuingRegion = scanIssuingRegion;
    self.scanLicenseNumber = scanLicenseNumber;
    self.scanLicenseType = scanLicenseType;
    self.scanLisenseSerial = scanLisenseSerial;
}

- (void)setScannerType:(int) scannerType {
    self.cardType = scannerType;
}

//
//  Delegates
//

- (void)scanResult:(OcrScanResult *)result {
    if( result == nil )
        return;

    if( result.result == 0 )
        [self.guidelayer changeGuideLayerState:0];
    else
        [self.guidelayer changeGuideLayerState:1];

    if( result.result == 3 )
    {
        [self.preview stopSession];
        [self performSelectorOnMainThread:@selector(sendResultDelegate:) withObject:result waitUntilDone:NO];
    }
    return;
}

- (void)sendResultDelegate:(OcrScanResult*) result {
    if( self.sdkViewDelegate != nil )
        [self.sdkViewDelegate ocrSDKView:self didScanCard:result];
}

@end
