//
//  Header.h
//  FinCubeOcrSDKv2
//
//  Created by FinCube on 2018. 9. 13..
//  Copyright © 2018년 FinCube. All rights reserved.
//

#ifndef Header_h
#define Header_h

/**
 * @brief   Card scanner result delegate
 * @details it send the card result to view controller
 * @author  FinCube
 * @date    2018.08.27
 * @version 2.0
 */
@class OcrScanResult;

@protocol OcrCameraPreviewDelegate<NSObject>

@required
- (void)scanResult:(OcrScanResult *)result;

@end

#endif /* Header_h */
