//
//  MenuViewController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/28.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    var parentVc: ViewController? = nil
    @IBOutlet weak var curLangButton: UIButton!
    @IBOutlet weak var conCurLangWidth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        /*
         let a = NSAttributedString(string: "A string", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)])
         print("size info : \(a.size())")
         let height = a.size().height
         */
        // test code
        //Россия | русский
        //대한민국 | 한국어
        let cur1 = NSAttributedString(string: "Россия | русский", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)])
        let cur2 = NSAttributedString(string: "대한민국 | 한국어", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)])
        print("size info : \(cur1.size())")
        print("size info : \(cur2.size())")
        //curLangButton.backgroundColor = UIColor.red
        curLangButton.setAttributedTitle(cur2, for: .normal)
        conCurLangWidth.constant = cur2.size().width + 30;
    }
    
    func setParentViewController(vc: ViewController) {
        self.parentVc = vc
    }
    
    @IBAction func onTest(_ sender: Any) {
        guard let pvc = parentVc else {
            return
        }
        
        pvc.doMenuAction()
    }
    
    @IBAction func onHome(_ sender: Any) {
    }
    
    @IBAction func onChangeLanguage(_ sender: Any) {
    }
    
    @IBAction func onNoti(_ sender: Any) {
    }
    
    @IBAction func onSetting(_ sender: Any) {
    }
    
    @IBAction func onClose(_ sender: Any) {
    }
}
