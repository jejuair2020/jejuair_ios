//
//  MenuPresenter.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/12/17.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol MenuDelegate {
    
}

class MenuPresenter {
    var viewDelegate: MenuDelegate? = nil
    
    required init(view: MenuDelegate?) {
        self.viewDelegate = view
    }
}
