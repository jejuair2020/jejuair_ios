//
//  IntroViewController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/27.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    private var presenter: IntroPresenter? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = IntroPresenter(view: self)
    }
}

extension IntroViewController: IntroDelegate {
    func serverStateInvalid(title: String, desc: String) {
        let alertController = UIAlertController(title: title, message: desc, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "확인", style: .cancel) { _ in
            exit(0)
        }
        alertController.addAction(cancelAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showIntroImage(imgUrl: String) {
        self.imageView.setImageUrlWIthFadeIn(url: imgUrl, sec: 0.4)
    }
    
    func finishedIntro() {
        //let t = self.presentingViewController
        //self.presentingViewController?.dismiss(animated: false, completion: nil)
        self.dismiss(animated: false, completion: nil)
        //self.navigationController?.popViewController(animated: false)
    }
}

