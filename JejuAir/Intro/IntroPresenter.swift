//
//  IntroPresenter.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/18.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//



import Foundation
import SwiftyJSON

protocol IntroDelegate {
    func serverStateInvalid(title: String, desc: String)
    func showIntroImage(imgUrl: String)
    func finishedIntro()
    
}
// https://japp.jejuair.net/jejuair
// /com/jeju/ibe/appMainCompInfo.do
// https://japp.jejuair.net/jejuair/com/jeju/ibe/appMainCompInfo.do
class IntroPresenter {
    var viewDelegate: IntroDelegate? = nil
    var fileUtil: FileUtil? = nil
    
    required init(view: IntroDelegate?) {
        self.viewDelegate = view
        self.fileUtil = FileUtil()
        self.checkServer()
    }
    
    // 서버 상태 체크
    func checkServer() {
        JNetwork.shared().checkServerState { (error, json) in
            if error == nil && json != nil {
                let result: JSON = json![GlobalManager.sharedInstance.userLanguage!]
                if result == JSON.null {
                    return
                }
                
                let flag: String  = result["flag"].stringValue == "" ? "N" : result["flag"].stringValue
                let title: String = result["title"].stringValue
                let desc: String  = result["desc"].stringValue
                
                if flag.isEqualToString(s: "Y") == true {
                    self.viewDelegate?.serverStateInvalid(title: title, desc: desc)
                    return
                }
            }
            self.getJejuAppData()
        }
    }
    
    // 인트로, 메인, 메뉴
    func getJejuAppData() {
        self.getIntroData()
    }
    
    // 인트로
    func getIntroData() {
        let url = "\(URL_BASE)\(URL_INTRODATA)?language=\(GlobalManager.sharedInstance.userLanguage!)"
        let fileName = "\(JSON_FILE_INTRO)\(GlobalManager.sharedInstance.userLanguage!)\(JSON_FILE_EXT)"
        JNetwork.shared().sendData(url: url, method: .post, parameters: nil) { (error, json) in
            if error == nil && json != nil {
                DispatchQueue.main.async {
                    self.viewDelegate?.showIntroImage(imgUrl: json!["introImgInfo"]["imgPath"].stringValue)
                }
                // save file
                if self.fileUtil?.saveJsonFile(destFoler: JSON_FOLDER, fileName: fileName, saveCont: json!.description) == false {
                    debugPrint("getIntroData() file save fail")
                }
                
                GlobalManager.sharedInstance.introJson = JSON(json!)
                debugPrint(GlobalManager.sharedInstance.introJson ?? "introJson is nil")
            }
            else {
                // restore file
                GlobalManager.sharedInstance.introJson = JSON(self.fileUtil?.restoreFromFile(sFolder: JSON_FOLDER, fileName: fileName) ?? "")
                DispatchQueue.main.async {
                    self.viewDelegate?.showIntroImage(imgUrl: GlobalManager.sharedInstance.introJson!["introImgInfo"]["imgPath"].stringValue)
                }
            }
            
            DispatchQueue.main.async {
                self.viewDelegate?.finishedIntro()
            }
        }
    }
}
