//
//  AirplaneViewController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/12/09.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

class AirplaneViewController: UIViewController {
    @IBOutlet weak var webView: CommonWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.webView
        guard let bundleUrl = Bundle.main.url(forResource: "test", withExtension: "html", subdirectory: "AirplaneRes/html") else {
            debugPrint("not search test.html")
            return
        }
        
        self.webView.loadFileUrl(url: bundleUrl)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
