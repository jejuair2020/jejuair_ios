

var JejuHybrid = function(){
    this.platform = "";
    this.native   = null;
    this.isApp    = false;
    
    var me = this;
    
    this.setHybridInfo = function(userAgent) {
        if (userAgent.indexOf("jappIos") >= 0) {
            me.platform = "iOS";
            me.isApp    = true;
            me.native   = new iOSInterface();
        }
        else if (userAgent.indexOf("jappAos") >= 0) {
            me.platform = "Android";
            me.isApp    = true;
            me.native   = new aOSInterface();
        }
        else {
            me.platform = "Browser";
            me.isApp    = false;
            me.native   = new browserInterface();
        }
    };
    
    this.exec = function(action, args, cbid) {
        if (me.native == null) {
            throw "not create native interface";
            return;
        }
        
        me.native.exec(action, args, cbid);
    };
    
    var iOSInterface = function() {
        this.exec = function(action, args, cbid) {
            if (webkit.messageHandlers === undefined) {
                throw "not support platform(iOS)";
                return;
            }
            var params = {
                "action" : action,
                "args" : args,
                "callbackId" : cbid
            };
            webkit.messageHandlers.JNative.postMessage(params);
        }
    };
    
    var aOSInterface = function() {
        this.exec = function(action, args, cbid) {
            if (JNative === undefined) {
                throw "not support platform(Android)";
                return;
            }
            
            var params = {
                "action" : action,
                "args" : args,
                "callbackId" : cbid
            };
            JNative.postMessage(JSON.stringify(params));
        }
    };
    var browserInterface = function() {
        this.exec = function(action, args, cbid) {
            throw "not support platform";
        }
    };

};

var hybrid = null;
var exec_id  = 0;
var exec_callbacks = {};

document.addEventListener("JejuReady", function(){
    hybrid = new JejuHybrid();
    
    hybrid.setHybridInfo(navigator.userAgent);
    if (typeof didInitHybrid == "function") {
        didInitHybrid();
    }
    
});

function isApp() {
    if (hybrid == null)
        return false;
    return hybrid.isApp;
}

function getPlatform() {
    if (hybrid == null)
        return "Browser";
    return hybrid.platform;
}

function NativeCall(action, args, success, failure) {
    if (hybrid == null) {
        throw "not create native interface";
        return;
    }
    
    var commandId = "";
    if (typeof success == 'function' || typeof failure == 'function') {
        commandId = "jcallback" + exec_id;
        exec_id++;
        exec_callbacks[commandId] = { success: success, failure: failure };
    }
    var inparams = {};
    if (typeof args != "undefined" && args != null && args.length > 0) {
        for (var i=0; i<args.length; i++) {
            var key = "arg"+(i+1);
            inparams[key] = args[i];
        }
    }
    hybrid.exec(action, inparams, commandId);
}

function jAppCallback(cinfo) {
    var callFunc = null;
    if (cinfo.result == "success") {
        if (typeof exec_callbacks[cinfo.callbackId].success == 'function') {
            callFunc = exec_callbacks[cinfo.callbackId].success;
        }
    } else {
        //alert(JSON.stringify(cinfo))
        if (typeof exec_callbacks[cinfo.callbackId].failure == 'function') {
            
            callFunc = exec_callbacks[cinfo.callbackId].failure;
        }
    }
    if (callFunc != null)
        callFunc(cinfo.args);
    
    delete exec_callbacks[cinfo.callbackId];
}
