//
//  MPassViewController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/12/11.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

class MPassViewController: BaseViewController {

    @IBOutlet weak var webView: CommonWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.webView.loadUrl(url: "https://www.jejuair.net/jejuair/kr/mobile/main.do")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
