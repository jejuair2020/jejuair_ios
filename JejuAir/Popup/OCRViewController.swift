//
//  OCRViewController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/12/04.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

protocol OCRViewDelegate {
    func didEndOCRDecode(result: OcrScanResult, scanType: Int32)
}

class OCRViewController: UIViewController {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var ocrPreView: OcrSdkView!
    
    var ocrScannerType: Int32 = 1 // creditCard : 0, passport : 5
    var scanExpiry: Bool = true
    var validateNumber: Bool = true
    var validateExpiry: Bool = true
    var delegate: OCRViewDelegate? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.ocrPreView.moveGuideRect(0.5, vertical: 0.5)
        self.ocrPreView.sdkViewDelegate = self
        self.ocrPreView.setScannerType(ocrScannerType)
        self.ocrPreView.setOptions(scanExpiry, validateNumber: validateNumber, validateExpiry: validateExpiry)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setPrepareData(sExpiry: Bool, vNumber: Bool, vExpiry: Bool, sType: Int32) {
        self.ocrScannerType = sType
        self.scanExpiry = sExpiry
        self.validateNumber = vNumber
        self.validateExpiry = vExpiry
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        //self.btnClose.bringSubviewToFront(self.ocrPreView)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OCRViewController: OcrSdkViewDelegate {
    func ocrSDKView(_ ocrSDKView: OcrSdkView, didScanCard result: OcrScanResult) {
        /*
        var resultString: String = ""
        if ocrScannerType == 0 {
            resultString = "result : \(result.result)\ncardNumber : \(result.cardNumber ?? "")\nexpiryDate : \(result.expiryDate ?? "")"
        } else if ocrScannerType == 5 {
            resultString = "result : \(result.result)\npassport_type : \(result.passport_type ?? "")\nname : \(result.name ?? "")\nname_kor: \(result.name_kor ?? "")\nkor_id_num : \(result.kor_id_num ?? "")\nissuing_country : \(result.issuing_country ?? "")\npassport_no : \(result.passport_no ?? "")\nnationality : \(result.nationality ?? "")\ndate_of_birth : \(result.date_of_birth ?? "")\nsex : \(result.sex ?? "")\nexpiryDate : \(result.expiryDate ?? "")\npersonal_no : \(result.personal_no ?? "")\nvalidate : \(result.validate)"
        }
        
        let alertController = UIAlertController(title: "", message: resultString, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "확인", style: .default) { _ in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        */
        self.dismiss(animated: true) {
            if self.delegate != nil {
                self.delegate?.didEndOCRDecode(result: result, scanType: self.ocrScannerType)
            }
        }
    }
}
