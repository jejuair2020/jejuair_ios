//
//  HybridErrors.swift
//  testweb
//
//  Created by WONJIN CHOI on 2019/11/11.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation

let HYBRID_ERROR_CODE_0000:String = "0000"
let HYBRID_ERROR_DESC_0000:String = "This feature is not supported."

let HYBRID_ERROR_CODE_0001:String = "0001"
let HYBRID_ERROR_DESC_0001:String = "Invalid parameters."

let HYBRID_ERROR_CODE_0002:String = "0002"
let HYBRID_ERROR_DESC_0002:String = "Required parameter missing."
