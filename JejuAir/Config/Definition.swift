//
//  Definition.swift
//  testweb
//
//  Created by WONJIN CHOI on 2019/11/06.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation

//<!-- 비행기모드 관련
// 앱 도큐먼트 폴더
let DOCUMENT_FOLDER: String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
// 비행기모드 html 파일 저장 폴더
let AIRPLANE_FOLDER: String = "airplane_res"
// 비행기모드 html 파일 저장 전체 경로
let AIRPLANE_FULLPATH: String = "\(DOCUMENT_FOLDER)/\(AIRPLANE_FOLDER)"
// 비행기보드 버전정보 파일 경로
let AIRPLANE_VERSION_FILE: String = "\(AIRPLANE_FULLPATH)/version.json"
// 현재 비행기모드 버전 정보
var AIRPLANE_CURRENT_VERSION: String = ""
// 앱 리소스에 포함된 파일 명
let AIRPLANE_REL_FILENAME: String = "airplane"
let AIRPLANE_REL_FILETYPE: String = ".zip"
//--> 비행기모드 관련

//<!-- json 저장 파일명
let JSON_FOLDER: String = "JSON"
let JSON_FILE_EXT: String = ".json"
let JSON_FILE_INTRO: String = "intro_"
//-->

//<!-- 웹뷰 관련
let WEBVIEW_APPEND_USERAGENT: String = "jappIos"
//-->

//<!-- API URL 정보
let URL_SCHEMA: String = "https"
var URL_DOMAIN: String = "japp.jejuair.net"
var URL_DOMAIN_SITE: String = "https://japp.jejuair.net"
var URL_LOG: String = "https://wiselog.jejuair.net:8443"
var URL_SYSTEM_CHECK: String = "http://sysinfo.jejuair.net/check/prod/check.json"
var URL_BASE: String = "\(URL_SCHEMA)://\(URL_DOMAIN)"

// 인트로 데이타
let URL_INTRODATA: String = "/jejuair/com/jeju/ibe/appMainCompInfo.do"
//-->

class Definition {
    static func setServerInfo(serverType: String?) {
        guard let info = serverType else {
            return
        }
        
        switch info {
        case "dev":
            URL_DOMAIN = "djapp5.jejuair.net"
            URL_DOMAIN_SITE = "https://djapp5.jejuair.net"
            URL_LOG = "https://twiselog.jejuair.net:8443"
            URL_SYSTEM_CHECK = "http://sysinfo.jejuair.net/check/dev/check.json"
            break
        case "t1":
            URL_DOMAIN = "tjapp1.jejuair.net"
            URL_DOMAIN_SITE = "https://tjapp1.jejuair.net"
            URL_LOG = "https://twiselog.jejuair.net:8443"
            URL_SYSTEM_CHECK = "http://sysinfo.jejuair.net/check/dev/check.json"
            break
            
        case "t2":
            URL_DOMAIN = "tjapp2.jejuair.net"
            URL_DOMAIN_SITE = "https://tjapp2.jejuair.net"
            URL_LOG = "https://twiselog.jejuair.net:8443"
            URL_SYSTEM_CHECK = "http://sysinfo.jejuair.net/check/dev/check.json"
            break
            
        case "t3":
            URL_DOMAIN = "tjapp3.jejuair.net"
            URL_DOMAIN_SITE = "https://tjapp3.jejuair.net"
            URL_LOG = "https://twiselog.jejuair.net:8443"
            URL_SYSTEM_CHECK = "http://sysinfo.jejuair.net/check/dev/check.json"
            break
        default:
            URL_DOMAIN = "\(info).jejuair.net"
            URL_DOMAIN_SITE = "https://\(info).jejuair.net"
            URL_LOG = "https://wiselog.jejuair.net:8443"
            URL_SYSTEM_CHECK = "http://sysinfo.jejuair.net/check/dev/check.json"
            break
        }
        
        URL_BASE = "\(URL_SCHEMA)://\(URL_DOMAIN)"
    }
}
