//
//  AppDelegate.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/27.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Use Firebase library to configure APIs
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "com.JejuAir.JejuAir"
        FirebaseApp.configure()
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        //jjairapp://launcher?server=dev
        if (url.scheme?.hasPrefix("jjairapp"))! {
            guard let host = url.host else {
                return true
            }
            
            if host == "launcher" {
                if let query = url.query {
                    let queryArr = query.split(separator: "&")
                    if queryArr.count > 0 {
                        for parameter in queryArr {
                            let parameterArr = parameter.split(separator: "=")
                            if parameterArr.count == 2 && parameterArr[0] == "server" {
                                let serverType = String(parameterArr[1])
                                Definition.setServerInfo(serverType: serverType)
                            }
                        }
                    }
                }
            }
        }
        return true
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

