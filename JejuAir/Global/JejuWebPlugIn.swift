//
//  JejuWebPlugIn.swift
//  testweb
//
//  Created by WONJIN CHOI on 2019/11/12.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit
import WebKit

class JejuWebPlugIn: NSObject {
    var webView:WKWebView?
    var parent:UIViewController?
    
    func executePlugIn(webView:WKWebView?, parent:UIViewController?, body:Any?) {
        self.webView = webView
        self.parent  = parent
        guard let bodyDic:[String:Any] = body as? Dictionary else {
            print("Invalid message body")
            return
        }
        let callbackId:String = bodyDic["callbackId"] as? String ?? ""
        guard let action:String = bodyDic["action"] as? String else {
            self.sendErrorCallback(callbackId: callbackId, msg: HYBRID_ERROR_DESC_0001, code: HYBRID_ERROR_CODE_0001)
            return
        }
        let params:Dictionary<String, Any>? = bodyDic["args"] as? Dictionary<String, Any> ?? nil
        
        if self.execute(action: action, params: params, cbId: callbackId) == false {
            self.sendErrorCallback(callbackId: callbackId, msg: HYBRID_ERROR_DESC_0000, code: HYBRID_ERROR_CODE_0000)
        }
    }
    
    func execute(action:String, params:Dictionary<String, Any>?, cbId:String) -> Bool {
        return false
    }
    
    func getWebView()->WKWebView? {
        return self.webView
    }
    
    func getParentViewController()->UIViewController? {
        return self.parent
    }
    
    func getStringParam(params:Dictionary<String, Any>?, key:String) -> String {
        guard let _:Dictionary<String, Any> = params, let value:String = params?[key] as? String else {
            return ""
        }
        return value
    }
    
    func getIntParam(params:Dictionary<String, Any>?, key:String) -> Int {
        guard let _:Dictionary<String, Any> = params, let value:Int = params?[key] as? Int else {
            return 0
        }
        
        return value
    }
    
    func getBoolParam(params:Dictionary<String, Any>?, key:String) -> Bool {
        guard let _:Dictionary<String, Any> = params, let value:Bool = params?[key] as? Bool else {
            return false
        }
        
        return value
    }
    
    func sendSuccessCallback(callbackId:String, result:Dictionary<String, Any>?) {
        if callbackId.isEmpty {
            return
        }
        var resultString:String = "{}"
        if let args:Dictionary<String, Any> = result {
            if let jsonString = args.toJSONString() {
                resultString = jsonString
            }
        }
        
        
        self.webView?.evaluateJavaScript("jAppCallback({result: 'success', callbackId: '\(callbackId)', args:\(resultString)})", completionHandler: nil)
    } 
    
    func sendErrorCallback(callbackId:String, msg:String, code:String) {
        if callbackId.isEmpty || self.webView == nil {
            return
        }
        var retDic = Dictionary<String, String>()
        retDic["msg"] = msg;
        retDic["code"] = code;
        self.webView?.evaluateJavaScript("jAppCallback({result: 'failure', args: \(retDic.toJSONString() ?? "{}") , callbackId: '\(callbackId)'})", completionHandler: nil)
    }
}
