//
//  GlobalManager.swift
//  testweb
//
//  Created by WONJIN CHOI on 2019/10/28.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON

final class GlobalManager: NSObject {
    static let sharedInstance: GlobalManager = GlobalManager()
    var processPool: WKProcessPool?
    var userLanguage: String?
    var userLocal: String?
    var introJson: JSON?
    
    override init() {
        self.processPool = WKProcessPool()
        self.userLanguage = "KR"
    }
}
