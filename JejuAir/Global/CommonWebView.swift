//
//  CommonWebView.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/29.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit
import WebKit

class CommonWebView: UIView {

    var webView : WKWebView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    private func commonInit() {
        let configuration: WKWebViewConfiguration = WKWebViewConfiguration()
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
        configuration.preferences = preferences
        configuration.processPool = GlobalManager.sharedInstance.processPool!
        
        let scriptString = "var JejuReady = new Event('JejuReady');document.dispatchEvent(JejuReady);"
        let userScript = WKUserScript(source: scriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        let contentController = WKUserContentController()
        contentController.addUserScript(userScript)
        contentController.add(self, name: "JNative")
        configuration.userContentController = contentController
        
        self.webView = WKWebView(frame: CGRect.zero, configuration: configuration)
        self.webView!.allowsLinkPreview = false
        self.webView!.uiDelegate = self
        self.webView!.navigationDelegate = self
        self.addSubview(self.webView!)
        self.webView!.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.size.equalToSuperview()
        }
        
        self.webView?.evaluateJavaScript("navigator.userAgent") {(result, error) in
            print (error ?? "no error")
            if let webView = self.webView, let userAgent = result as? String {
                webView.customUserAgent = "\(userAgent) \(WEBVIEW_APPEND_USERAGENT)"
            }
        }
    }
    
    func loadFile(path:String) {
        //self.addWebView()
        let htmlUrl = URL(fileURLWithPath: path, isDirectory: false)
        self.webView?.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
    }
    
    func loadFileUrl(url:URL) {
        self.webView?.loadFileURL(url, allowingReadAccessTo: url)
    }
    
    func loadUrl(url:String) {
        let link = URL(string:url)!
        let request = URLRequest(url: link)
        self.webView?.load(request)
    }

}

extension CommonWebView: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //webView.evaluateJavaScript("lang('\(GlobalManager.sharedInstance.userLanguage ?? "")')") { (result, error) in
            //print(error ?? "success")
        //}
    }
}

extension CommonWebView: WKUIDelegate {
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
           
       if let parentVC = self.parentViewController {
           let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
           let cancelAction = UIAlertAction(title: "확인", style: .cancel) { _ in
               completionHandler()
           }
           alertController.addAction(cancelAction)
           DispatchQueue.main.async {
               parentVC.present(alertController, animated: true, completion: nil)
           }
       }
       else {
           completionHandler()
       }
    }
       
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        if let parentVC = self.parentViewController {
            let alertController = UIAlertController(title: "test", message: message, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "취소", style: .cancel) { _ in
                completionHandler(false)
            }
            let okAction = UIAlertAction(title: "확인", style: .default) { _ in
                completionHandler(true)
            }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            DispatchQueue.main.async {
                parentVC.present(alertController, animated: true, completion: nil)
            }
        }
    }
}

extension CommonWebView: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "JNative" {
            guard let bodyDic:[String:Any] = message.body as? Dictionary else {
                return
            }
            guard let action:String = bodyDic["action"] as? String else {
                return
            }
            
            if action == "openSystemWebView"
                || action == "openInAppWebView"
                || action == "close"
                || action == "closeToRoot"
                || action == "openMenu" {
                UtilPlugIn.sharedInstance.executePlugIn(webView: self.webView, parent: self.parentViewController, body: message.body)
            }
            
            if action == "openOcrPassport" || action == "openOcrCardNumber" {
                OCRPlugIn.sharedInstance.executePlugIn(webView: self.webView, parent: self.parentViewController, body: message.body)
            }
        }
    }
}
