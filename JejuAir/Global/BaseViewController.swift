//
//  BaseViewController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/27.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showMenu() {
        guard let navi: JNaviController = self.navigationController as? JNaviController else {
            return
        }
        
        navi.showMenu()
    }
     

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
