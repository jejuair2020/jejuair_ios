//
//  JNaviController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/14.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

class JNaviController: UINavigationController {
    
    var parentVc: ViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.isNavigationBarHidden = true
    }
    
    func setParentViewController(vc: ViewController) {
        self.parentVc = vc
    }
    
    func showMenu() {
        guard let vc: ViewController = parentVc else {
            return
        }
        
        vc.showMenu()
    }
    
    func getTabBarController() -> JTabBarController {
        return self.viewControllers.first as! JTabBarController
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
