//
//  JNetwork.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/18.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class JNetwork {
    static let sharedInstance:JNetwork = JNetwork()
    
    class func shared() -> JNetwork {
        return sharedInstance
    }
    
    // 서버 상태 확인
    static var persons: [String] = []
    func checkServerState(completion: @escaping (Error?, JSON?) -> Void) {
        self.sendData(url: URL_SYSTEM_CHECK, method: .get, parameters: nil, completion: completion)
        /*
        Alamofire.request(URL_SYSTEM_CHECK, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch (response.result) {
                case .success(let value):
                    completion(nil, JSON(value))
                    break
                case .failure(let error):
                    completion(error, nil)
                    break
                }
            }
        */
    }
    
    
    func sendData(url: String, method: HTTPMethod, parameters: Dictionary<String, Any>?, completion: @escaping (Error?, JSON?) -> Void) {
        var params: Dictionary<String, Any> = parameters ?? Dictionary<String, Any>()
        // 필수 파라메터
        params["language"] = GlobalManager.sharedInstance.userLanguage!
        Alamofire.request(url, method: method, parameters: params, encoding: JSONEncoding.default)
        .responseJSON { response in
            switch (response.result) {
            case .success(let value):
                completion(nil, JSON(value))
                break
            case .failure(let error):
                completion(error, nil)
                break
            }
        }
    }
}
