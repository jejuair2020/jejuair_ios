//
//  JTabBarController.swift
//  JejuAir
//
//  Created by WONJIN CHOI on 2019/11/14.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UIKit

class JTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        UITabBar.appearance().tintColor = UIColor.JEJUCOLOR.TABTINT
        UITabBar.appearance().unselectedItemTintColor = UIColor.JEJUCOLOR.TABTINTUNSELECTED
        tabBar.barTintColor = UIColor.white
        
        let bottomSafeAreaHeight: CGFloat = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: UIColor.JEJUCOLOR.TABSELECTION!, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height+bottomSafeAreaHeight), lineHeight: 2.0)
        
        
        //tabBar.isHidden = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
