//
//  NotificationService.swift
//  NotificationService
//
//  Created by WONJIN CHOI on 2019/12/09.
//  Copyright © 2019 WONJIN CHOI. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        let attachmentUrlString = request.content.userInfo["imgUrl"]
        let url = URL(string: attachmentUrlString as! String)
        
        if url == nil {
            contentHandler(bestAttemptContent!)
            return
        }
        
        let downloadTask = URLSession.shared.downloadTask(with: url!, completionHandler: {(location ,response, error) -> Void in
            if error == nil {
                let tempDict = NSTemporaryDirectory()
                var attachmentID = UUID().uuidString + (response?.url?.lastPathComponent)!
                
                if (response?.suggestedFilename != nil) {
                    attachmentID = UUID().uuidString + (response?.suggestedFilename)!
                }
                
                let tempFilePath = tempDict + attachmentID
                let urlTempFile = URL(fileURLWithPath: tempFilePath)
                try! FileManager.default.moveItem(atPath: (location?.path)!, toPath: tempFilePath)
                
                if let attachment = try? UNNotificationAttachment(identifier: "", url: urlTempFile) {
                    self.bestAttemptContent?.attachments = [attachment]
                }
            }
            
            OperationQueue.main.addOperation({
                contentHandler(self.bestAttemptContent!)
            })
            
        })
        downloadTask.resume()
        
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    /*
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            bestAttemptContent.title = "\(bestAttemptContent.title) [modified]"
            
            contentHandler(bestAttemptContent)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    */
}
